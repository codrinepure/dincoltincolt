#include <iostream>
#include <graphics.h>
#include<stdio.h>
#include<windows.h>
#include<MMSystem.h>

#define MAX 20
#define FUNDAL BLACK
#define SPATIU -1
#define PIESA1 WHITE
#define PIESA2 BLACK

#define FUNDAL1 LIGHTCYAN
#define FUNDAL2 RED


using namespace std;



int stanga,sus,width,height,latura, numar;
bool gata;

int TablaDeJoc[MAX][MAX];// TABLA DE JOC

int collc[8][8];// MATRICE PENTRU RETINEREA CULORII FIECARUI PATRATEL DIN TABLA DE JOC

int scor;






int VictorieAlb() // FUNCTIE PENTRU VERIFICARE DACA ALBUL A AJUNS IN POZITIE FINALA
{
    if(TablaDeJoc[1][6]==WHITE&&TablaDeJoc[1][7]==WHITE&&TablaDeJoc[1][8]==WHITE&&TablaDeJoc[2][7]==WHITE&&TablaDeJoc[2][8]==WHITE&& TablaDeJoc[3][8]==WHITE)
        return 1;
    else return 0;
}
int VictorieNegru() //FUNCTIE PENTRU VERIFICARE DACA NEGRUL A AJUNS IN POZITIE FINALA
{
    if(TablaDeJoc[6][1]==BLACK && TablaDeJoc[7][1]==BLACK && TablaDeJoc[7][2]==BLACK && TablaDeJoc[8][1]==BLACK && TablaDeJoc[8][2]==BLACK  && TablaDeJoc[8][3]==BLACK)
        return 1;
    else return 0;
}


void ReseteazaTabla(int linia, int coloana, int fundal) // FUNCTIE PENTRU A RESETA TABLA DUPA CE SE ARATA POSIBILITATILE DE MUTARE A UNEI PIESE
{
    int x1,y1,x2,y2;
   x1=stanga+latura*(coloana-1);
   y1=sus+latura*(linia-1);
   x2=x1+latura;
   y2=y1+latura;

    rectangle(x1,y1,x2,y2);
    setcolor(fundal);


   setfillstyle(SOLID_FILL,fundal);
   bar(x1,y1,x2,y2);

}

void deseneaza(int linia, int coloana, int culoare1, int culoare2) // FUNCTIE PENTRU A ARATA SOLUTIILE DE MUTARE A UNEI PIESE
{
    int x1,x2,y1,y2;

    x1=stanga+latura*(coloana-1);
   y1=sus+latura*(linia-1);
   x2=x1+latura;
   y2=y1+latura;

   setfillstyle(SOLID_FILL,culoare1);
   bar(x1,y1,x2,y2);

    setcolor(culoare2);
   rectangle(x1,y1,x2,y2);



}

void ArataSol(int l, int c, int cod) //FUNCTIE CARE ARATA SOLUTIILE DE MUTARE
{
    int i,lv,cv;
    int dl1[]={-1,-1,-1,0,1,1,1,0};
    int dc1[]={-1,0,1,1,1,0,-1,-1};
    int dl2[]={-2,-2,-2,0,2,2,2,0};
    int dc2[]={-2,0,2,2,2,0,-2,-2};

    if(cod==1)
    {
        for(i=0;i<8;i++)
        {
        lv=l+dl1[i];
        cv=c+dc1[i];
        if(TablaDeJoc[lv][cv]==SPATIU)
            deseneaza(lv,cv,LIGHTRED,BLACK);
        }

        for(i=0;i<8;i++)
        {
            lv=l+dl2[i];
            cv=c+dc2[i];
            if(TablaDeJoc[lv][cv]==SPATIU)
                if((lv==l-2 && cv==c && TablaDeJoc[l-1][c]!=SPATIU) ||
                     (lv==l-2 && cv==c+2 && TablaDeJoc[l-1][c+1]!=SPATIU) ||
                     (lv==l && cv==c+2 && TablaDeJoc[l][c+1]!=SPATIU) ||
                     (lv==l+2 && cv==c+2 && TablaDeJoc[l+1][c+1]!=SPATIU) ||
                     (lv==l+2 && cv==c && TablaDeJoc[l+1][c]!=SPATIU) ||
                     (lv==l+2 && cv==c-2 && TablaDeJoc[l+1][c-1]!=SPATIU) ||
                     (lv==l && cv==c-2 && TablaDeJoc[l][c-1]!=SPATIU) ||
                     (lv==l-2 && cv==c-2 && TablaDeJoc[l-1][c-1]!=SPATIU))
                     deseneaza(lv,cv,YELLOW,BLACK);

        }

    }
    else
    {
            for(i=0;i<8;i++)
        {
            lv=l+dl1[i];
            cv=c+dc1[i];
            if(TablaDeJoc[lv][cv]==SPATIU)
                ReseteazaTabla(lv,cv,collc[lv][cv]);
        }

        for(i=0;i<8;i++)
        {
            lv=l+dl2[i];
            cv=c+dc2[i];
            if(TablaDeJoc[lv][cv]==SPATIU)
                if((lv==l-2 && cv==c && TablaDeJoc[l-1][c]!=SPATIU) ||
                     (lv==l-2 && cv==c+2 && TablaDeJoc[l-1][c+1]!=SPATIU) ||
                     (lv==l && cv==c+2 && TablaDeJoc[l][c+1]!=SPATIU) ||
                     (lv==l+2 && cv==c+2 && TablaDeJoc[l+1][c+1]!=SPATIU) ||
                     (lv==l+2 && cv==c && TablaDeJoc[l+1][c]!=SPATIU) ||
                     (lv==l+2 && cv==c-2 && TablaDeJoc[l+1][c-1]!=SPATIU) ||
                     (lv==l && cv==c-2 && TablaDeJoc[l][c-1]!=SPATIU) ||
                     (lv==l-2 && cv==c-2 && TablaDeJoc[l-1][c-1]!=SPATIU))
                     ReseteazaTabla(lv,cv,collc[lv][cv]);



        }

    }



}

void stergePiesa(int linia, int coloana, int fundal) // FUNCTIE DE STERGERE PIESA
{
   int x1,y1,x2,y2,xmijloc,ymijloc;
   x1=stanga+latura*(coloana-1);
   y1=sus+latura*(linia-1);
   x2=x1+latura;
   y2=y1+latura;
   xmijloc=(x1+x2)/2;
   ymijloc=(y1+y2)/2;
   rectangle(x1,y1,x2,y2);
   setcolor(fundal);
   setfillstyle(SOLID_FILL,fundal);
   bar(xmijloc-26,ymijloc-26,xmijloc+26,ymijloc+26);
}

int AfisareNumarMutareAlb(int PunctajAlb) // FUNCTIE PENTRU AFISAREA PUNCTAJULUI LA MUTARILE ALBULUI
{
        setcolor(RED);
        char ContorMutariAlb[20]="Mutari Alb ";
        sprintf(ContorMutariAlb + 11, "%d", PunctajAlb);
        outtextxy(1000,100,ContorMutariAlb);


}

int AfisareNumarMutareAlb2(int PunctajAlb) // FUNCTIE PENTRU AFISAREA PUNCTAJULUI LA MUTARILE ALBULUI IN GAME DEMONSTRATION
{
        setcolor(RED);
        char ContorMutariAlb[20]="Mutari Alb ";
        sprintf(ContorMutariAlb + 11, "%d", PunctajAlb);
        settextstyle(10, HORIZ_DIR, 5);
        outtextxy(1000,100,ContorMutariAlb);


}
int AfisareNumarMutareNegru(int PunctajNegru) // FUNCTIE PENTRU AFISAREA PUNCTAJULUI LA MUTARILE NEGRULUI
{
        setcolor(RED);
        char ContorMutariNegru[20]="Mutari Negru ";
        sprintf(ContorMutariNegru + 13, "%d", PunctajNegru);
        outtextxy(1000,500,ContorMutariNegru);
}

int AfisareNumarMutareNegru2(int PunctajNegru) // FUNCTIE PENTRU AFISAREA PUNCTAJULUI LA MUTARILE NEGRULUI IN GAME DEMONSTRATION
{
        setcolor(RED);
        char ContorMutariNegru[20]="Mutari Negru ";
        sprintf(ContorMutariNegru + 13, "%d", PunctajNegru);
        settextstyle(10, HORIZ_DIR, 5);
        outtextxy(1000,500,ContorMutariNegru);
}





void deseneazaPiesa(int linia, int coloana, int codPiesa)
{
   int x1,y1,x2,y2,xmijloc,ymijloc;
   x1=stanga+latura*(coloana-1);
   y1=sus+latura*(linia-1);
   x2=x1+latura;
   y2=y1+latura;
   xmijloc=(x1+x2)/2;
   ymijloc=(y1+y2)/2;

   setcolor(codPiesa);
   setfillstyle(SOLID_FILL,codPiesa);
   fillellipse(xmijloc,ymijloc,25,25);
}

bool inInterior(int x, int y, int x1, int y1, int x2, int y2)// FUNCTIE CARE VERIFICA DACA CLICK UL ESTE IN INTERIORUL TABLEI
{
   return x1<=x && x<=x2 && y1<=y && y<=y2;
}




void initTabla() // FUNCTIE PENTRU INITIEREA TABLEI DE JOC
{
 numar=8;
 int i, j;


 for (i=1; i<=numar; i++)
     for (j=1; j<=numar; j++)
            TablaDeJoc[i][j]=SPATIU;

 // piesele negre
 TablaDeJoc[1][6]=TablaDeJoc[1][7]=TablaDeJoc[1][8]=PIESA2;
 TablaDeJoc[2][7]=TablaDeJoc[2][8]=PIESA2;
 TablaDeJoc[3][8]=PIESA2;

 //piesele albe

  TablaDeJoc[6][1]=PIESA1;
 TablaDeJoc[7][1]=TablaDeJoc[7][2]=PIESA1;
 TablaDeJoc[8][1]=TablaDeJoc[8][2]=TablaDeJoc[8][3]=PIESA1;


}



void desen() // FUNCTIE PENTRU DESENAREA TABLE DE JOC
{
   int linia,coloana;
   numar=8;

   width=600;
   height=600;
   latura=width/numar;

   sus=height*10/100; stanga=20*width/100;


   setbkcolor(FUNDAL); clearviewport();




    for (linia=1; linia<=numar; linia++)
       {
           for (coloana=1; coloana<=numar; coloana++)
            {
                if((linia+coloana) % 2==0)
                    {setfillstyle(SOLID_FILL,FUNDAL1); collc[linia][coloana]=FUNDAL1;}
                else
                    {setfillstyle(SOLID_FILL,FUNDAL2); collc[linia][coloana]=FUNDAL2;}

            bar(stanga+latura*(linia-1),sus+latura*(coloana-1),stanga+latura*linia,sus+latura*coloana);

            }


       }

            int primlin=sus+30;

            settextstyle(7, HORIZ_DIR, 3);
            setcolor(FUNDAL1);

             outtextxy(stanga-70,primlin,"8"); primlin+=latura;
             outtextxy(stanga-70,primlin,"7"); primlin+=latura;
             outtextxy(stanga-70,primlin,"6"); primlin+=latura;
             outtextxy(stanga-70,primlin,"5"); primlin+=latura;
             outtextxy(stanga-70,primlin,"4"); primlin+=latura;
             outtextxy(stanga-70,primlin,"3"); primlin+=latura;
             outtextxy(stanga-70,primlin,"2"); primlin+=latura;
             outtextxy(stanga-70,primlin,"1"); primlin+=latura;

            int primcol=stanga+30;

            outtextxy(primcol,height+100,"A"); primcol+=latura;
            outtextxy(primcol,height+100,"B"); primcol+=latura;
            outtextxy(primcol,height+100,"C"); primcol+=latura;
            outtextxy(primcol,height+100,"D"); primcol+=latura;
            outtextxy(primcol,height+100,"E"); primcol+=latura;
            outtextxy(primcol,height+100,"F"); primcol+=latura;
            outtextxy(primcol,height+100,"G"); primcol+=latura;
            outtextxy(primcol,height+100,"H"); primcol+=latura;




   for (linia=1; linia<=numar; linia++)
       for (coloana=1; coloana<=numar; coloana++)
       {
         if (TablaDeJoc[linia][coloana]!=SPATIU)
            deseneazaPiesa(linia,coloana,TablaDeJoc[linia][coloana]);




       }
}

void patrat(int x, int y, int l) // FUNCTIE FOLOSITA LA MENIU PENTRU A CREA CHENARUL
{
    line(x-l/2,y-l/2,x+l/2,y-l/2);
    line(x+l/2,y-l/2,x+l/2,y+l/2);
    line(x+l/2,y+l/2,x-l/2,y+l/2);
    line(x-l/2,y+l/2,x-l/2,y-l/2);
}

void deseneazaPiesaMeniu(int x, int y, int raza,int culoare) // FUNCTIE CU CARE SE DESEANAZA BULINELE DIN MENIU
{
   setcolor(culoare);
   setfillstyle(SOLID_FILL,culoare);
   fillellipse(x,y,raza,raza);
}


void CreareButonText(int x1, int y1, int x2, int y2, int borderc, int fillc, int textc, char text[]) // FUNCTIE PENTRU BUTOANE
{
    setcolor(borderc);
    setlinestyle(SOLID_LINE, 0xFFFF, THICK_WIDTH);
    setfillstyle(SOLID_FILL, fillc);
    rectangle(x1, y1, x2, y2);
    floodfill(int((x1 + x2) / 2), int((y1 + y2) / 2), borderc);
    setbkcolor(fillc);
    setcolor(textc);
    settextjustify(CENTER_TEXT, CENTER_TEXT);
    settextstyle(EUROPEAN_FONT, HORIZ_DIR, 4);
    outtextxy(int((x1 + x2) / 2), int((y1 + y2) / 2), text);
}



void Meniu(int w, int h) // FUNCTIE PENTRU INTERFATA MENIULUI
{


    setbkcolor(BLACK); clearviewport();
    // titlul
     settextstyle(10, HORIZ_DIR, 7);
     setcolor(FUNDAL1);
     settextjustify(CENTER_TEXT, CENTER_TEXT);

     outtextxy(w/2,70,"Din Colt in Colt");


    setcolor(FUNDAL1);
    line(300,80,900,80);

    //titlul



    //chenarul format di patrate
        int x,y,r,n,l=550,c=1;
        n=10;r=10;
        x=w/2;y=h/2+50;



        while(n)
        {
               setcolor(c);c++;
                patrat(x,y,l);
                n--;
                l+=r;
        }

        //chenarul format din patrate

        //bulinele stanga

        int p=1;
        x=90;y=100;c=1;
        r=45;
        n=8;


        while(n)
        {
            deseneazaPiesaMeniu(x,y,r,c);
            if(p==1)
            {
                x+=100;
                p=0;
            }
            else
            {
                x-=100;
                p=1;
            }

             c++;
             n--;
             y+=90;
        }

        //bulinele stanga


        //bulinele dreapta

        x=1120;y=100;c=8;p=1;n=8;
        while(n)
        {
            deseneazaPiesaMeniu(x,y,r,c);
            if(p==1)
            {
                x-=100;
                p=0;
            }
            else
            {
                x+=100;
                p=1;
            }

             c--;
             n--;
             y+=90;
        }

        //bulinele dreapta



}

bool verificspatiugol2(int l, int c, int la, int ca) // FUNCTIE FOLOSITA LA SALTUL CONSECUTIV, VERIFICA DACA EXISTA POZITII VALIDE DE SALT
{
    int lv,cv,i;


    int dl2[]={-2,-2,-2,0,2,2,2,0};
    int dc2[]={-2,0,2,2,2,0,-2,-2};


        int x=TablaDeJoc[la][ca];
        TablaDeJoc[la][ca]=100;

        for(i=0;i<8;i++)
        {
            lv=l+dl2[i];
            cv=c+dc2[i];
            if(TablaDeJoc[lv][cv]==SPATIU)
                if((lv==l-2 && cv==c && TablaDeJoc[l-1][c]!=SPATIU) ||
                     (lv==l-2 && cv==c+2 && TablaDeJoc[l-1][c+1]!=SPATIU) ||
                     (lv==l && cv==c+2 && TablaDeJoc[l][c+1]!=SPATIU) ||
                     (lv==l+2 && cv==c+2 && TablaDeJoc[l+1][c+1]!=SPATIU) ||
                     (lv==l+2 && cv==c && TablaDeJoc[l+1][c]!=SPATIU) ||
                     (lv==l+2 && cv==c-2 && TablaDeJoc[l+1][c-1]!=SPATIU) ||
                     (lv==l && cv==c-2 && TablaDeJoc[l][c-1]!=SPATIU) ||
                     (lv==l-2 && cv==c-2 && TablaDeJoc[l-1][c-1]!=SPATIU))


                     {TablaDeJoc[la][ca]=x;return 1;}

        }

        TablaDeJoc[la][ca]=x;
        return 0;

}


void ArataSol2(int l, int c, int cod, int la, int ca)// ARATA POSIBILITATILE DE MUTARE PENTRU SALTUL CONSECUTIV
{
    int i,lv,cv;

    int dl1[]={-1,-1,-1,0,1,1,1,0};
    int dc1[]={-1,0,1,1,1,0,-1,-1};
    int dl2[]={-2,-2,-2,0,2,2,2,0};
    int dc2[]={-2,0,2,2,2,0,-2,-2};

    int x=TablaDeJoc[la][ca];
    TablaDeJoc[la][ca]=100;

    if(cod==1)
    {



        for(i=0;i<8;i++)
        {
            lv=l+dl2[i];
            cv=c+dc2[i];
            if(TablaDeJoc[lv][cv]==SPATIU)
                if((lv==l-2 && cv==c && TablaDeJoc[l-1][c]!=SPATIU) ||
                     (lv==l-2 && cv==c+2 && TablaDeJoc[l-1][c+1]!=SPATIU) ||
                     (lv==l && cv==c+2 && TablaDeJoc[l][c+1]!=SPATIU) ||
                     (lv==l+2 && cv==c+2 && TablaDeJoc[l+1][c+1]!=SPATIU) ||
                     (lv==l+2 && cv==c && TablaDeJoc[l+1][c]!=SPATIU) ||
                     (lv==l+2 && cv==c-2 && TablaDeJoc[l+1][c-1]!=SPATIU) ||
                     (lv==l && cv==c-2 && TablaDeJoc[l][c-1]!=SPATIU) ||
                     (lv==l-2 && cv==c-2 && TablaDeJoc[l-1][c-1]!=SPATIU))


                     {
                           deseneaza(lv,cv,YELLOW,BLACK);
                         TablaDeJoc[la][ca]=x;


                     }

        }

    }
    else
    {




        for(i=0;i<8;i++)
        {
            lv=l+dl2[i];
            cv=c+dc2[i];
            if(TablaDeJoc[lv][cv]==SPATIU)
                if((lv==l-2 && cv==c && TablaDeJoc[l-1][c]!=SPATIU) ||
                     (lv==l-2 && cv==c+2 && TablaDeJoc[l-1][c+1]!=SPATIU) ||
                     (lv==l && cv==c+2 && TablaDeJoc[l][c+1]!=SPATIU) ||
                     (lv==l+2 && cv==c+2 && TablaDeJoc[l+1][c+1]!=SPATIU) ||
                     (lv==l+2 && cv==c && TablaDeJoc[l+1][c]!=SPATIU) ||
                     (lv==l+2 && cv==c-2 && TablaDeJoc[l+1][c-1]!=SPATIU) ||
                     (lv==l && cv==c-2 && TablaDeJoc[l][c-1]!=SPATIU) ||
                     (lv==l-2 && cv==c-2 && TablaDeJoc[l-1][c-1]!=SPATIU))



                     {
                          ReseteazaTabla(lv,cv,collc[lv][cv]);
                          TablaDeJoc[la][ca]=x;

                     }



        }

    }



}


void mutarePiesa2(int codPiesa, int &cod, int &l, int &c, int &la, int &ca)// FUNCTIE PENTRU MUTARE PIESA LA SALT CONSECUTIV
{
    bool mutareCorecta;
   int linia1,coloana1,linia2,coloana2,x,y;
   int x1, y1, x2, y2;
   int xmijloc, ymijloc;
   bool F;
  do
   {

   mutareCorecta=false;
   if(ismouseclick(WM_LBUTTONDOWN) && inInterior(x=mousex(),y=mousey(),stanga,sus,stanga+width,sus+height))
   {
     clearmouseclick(WM_LBUTTONDOWN);

    linia1=(y-sus)/latura+1;
    coloana1=(x-stanga)/latura+1;


    if (TablaDeJoc[linia1][coloana1]==codPiesa)
    {
         la=linia1;
        ca=coloana1;
        ArataSol2(linia1,coloana1,1, la,ca);


      do
      {

        if(ismouseclick(WM_LBUTTONDOWN))
        {
            x=mousex();y=mousey();
            if(x>=800 && x<=950 && y>=700 && y<=750)
            {
                 clearmouseclick(WM_LBUTTONDOWN);
                mutareCorecta=true;
                ArataSol2(linia1,coloana1,2, la,ca);
                cod=0;
                return;
            }
            else if(inInterior(x,y,stanga,sus,stanga+width,sus+height))
            {
                clearmouseclick(WM_LBUTTONDOWN);

          linia2=(y-sus)/latura+1;
          coloana2=(x-stanga)/latura+1;


             ArataSol2(linia1,coloana1,2, la,ca);

               if (TablaDeJoc[linia2][coloana2]==SPATIU )
              if( (linia2==linia1-1 && coloana2==coloana1-1) || (linia2==linia1-1 && coloana2==coloana1+1) || (linia2==linia1+1 && coloana2==coloana1-1) || (linia2==linia1+1 && coloana2==coloana1+1 )|| (linia2==linia1-1 && coloana2==coloana1) || (linia2==linia1 && coloana2==coloana1+1) || (linia2==linia1+1 && coloana2==coloana1) || (linia2==linia1 && coloana2==coloana1-1))
          {
            cod=1;
            mutareCorecta=true;
            TablaDeJoc[linia1][coloana1]=SPATIU;
            TablaDeJoc[linia2][coloana2]=codPiesa;
            stergePiesa(linia1,coloana1,collc[linia1][coloana1]);
            deseneazaPiesa(linia2,coloana2,codPiesa);

          }



          if(TablaDeJoc[linia2][coloana2]==SPATIU)
              if((linia2==linia1-2 && coloana2==coloana1 && TablaDeJoc[linia1-1][coloana1]!=SPATIU) ||
                 (linia2==linia1-2 && coloana2==coloana1+2 && TablaDeJoc[linia1-1][coloana1+1]!=SPATIU) ||
                 (linia2==linia1 && coloana2==coloana1+2 && TablaDeJoc[linia1][coloana1+1]!=SPATIU) ||
                 (linia2==linia1+2 && coloana2==coloana1+2 && TablaDeJoc[linia1+1][coloana1+1]!=SPATIU) ||
                 (linia2==linia1+2 && coloana2==coloana1 && TablaDeJoc[linia1+1][coloana1]!=SPATIU) ||
                 (linia2==linia1+2 && coloana2==coloana1-2 && TablaDeJoc[linia1+1][coloana1-1]!=SPATIU) ||
                 (linia2==linia1 && coloana2==coloana1-2 && TablaDeJoc[linia1][coloana1-1]!=SPATIU) ||
                 (linia2==linia1-2 && coloana2==coloana1-2 && TablaDeJoc[linia1-1][coloana1-1]!=SPATIU))

                    {
                        l=linia2;
                        c=coloana2;
                            cod=2;
                            mutareCorecta=true;
                            TablaDeJoc[linia1][coloana1]=SPATIU;
                            TablaDeJoc[linia2][coloana2]=codPiesa;
                            stergePiesa(linia1,coloana1,collc[linia1][coloana1]);
                            deseneazaPiesa(linia2,coloana2,codPiesa);
                    }

        }
            }


      }
      while (!mutareCorecta);
    }
  }
 }
 while (!mutareCorecta);
}



void mutarePiesa(int codPiesa, int &cod, int &l, int &c, int &la, int &ca)// FUNCTIE PENTRU MUTARE PIESA
{
   int linia1,coloana1,linia2,coloana2,x,y;
   int x1, y1, x2, y2;
   int xmijloc, ymijloc;
   bool mutareCorecta;
  do
   {

   mutareCorecta=false;
   if(ismouseclick(WM_LBUTTONDOWN) && inInterior(x=mousex(),y=mousey(),stanga,sus,stanga+width,sus+height))
   {
     clearmouseclick(WM_LBUTTONDOWN);

    linia1=(y-sus)/latura+1;
    coloana1=(x-stanga)/latura+1;


    if (TablaDeJoc[linia1][coloana1]==codPiesa)
    {
          la=linia1;
        ca=coloana1;
        ArataSol(linia1,coloana1,1);


      do
      {

        if(ismouseclick(WM_LBUTTONDOWN) && inInterior(x=mousex(),y=mousey(),stanga,sus,stanga+width,sus+height))
        {
          clearmouseclick(WM_LBUTTONDOWN);

          linia2=(y-sus)/latura+1;
          coloana2=(x-stanga)/latura+1;


             ArataSol(linia1,coloana1,2);

          if (TablaDeJoc[linia2][coloana2]==SPATIU )
              if( (linia2==linia1-1 && coloana2==coloana1-1) || (linia2==linia1-1 && coloana2==coloana1+1) || (linia2==linia1+1 && coloana2==coloana1-1) || (linia2==linia1+1 && coloana2==coloana1+1 )|| (linia2==linia1-1 && coloana2==coloana1) || (linia2==linia1 && coloana2==coloana1+1) || (linia2==linia1+1 && coloana2==coloana1) || (linia2==linia1 && coloana2==coloana1-1))
          {
            cod=1;
            mutareCorecta=true;
            TablaDeJoc[linia1][coloana1]=SPATIU;
            TablaDeJoc[linia2][coloana2]=codPiesa;
            stergePiesa(linia1,coloana1,collc[linia1][coloana1]);
            deseneazaPiesa(linia2,coloana2,codPiesa);

          }



          if(TablaDeJoc[linia2][coloana2]==SPATIU)
              if((linia2==linia1-2 && coloana2==coloana1 && TablaDeJoc[linia1-1][coloana1]!=SPATIU) ||
                 (linia2==linia1-2 && coloana2==coloana1+2 && TablaDeJoc[linia1-1][coloana1+1]!=SPATIU) ||
                 (linia2==linia1 && coloana2==coloana1+2 && TablaDeJoc[linia1][coloana1+1]!=SPATIU) ||
                 (linia2==linia1+2 && coloana2==coloana1+2 && TablaDeJoc[linia1+1][coloana1+1]!=SPATIU) ||
                 (linia2==linia1+2 && coloana2==coloana1 && TablaDeJoc[linia1+1][coloana1]!=SPATIU) ||
                 (linia2==linia1+2 && coloana2==coloana1-2 && TablaDeJoc[linia1+1][coloana1-1]!=SPATIU) ||
                 (linia2==linia1 && coloana2==coloana1-2 && TablaDeJoc[linia1][coloana1-1]!=SPATIU) ||
                 (linia2==linia1-2 && coloana2==coloana1-2 && TablaDeJoc[linia1-1][coloana1-1]!=SPATIU))

                    {
                            l=linia2;
                            c=coloana2;
                            cod=2;
                            mutareCorecta=true;
                            TablaDeJoc[linia1][coloana1]=SPATIU;
                            TablaDeJoc[linia2][coloana2]=codPiesa;
                            stergePiesa(linia1,coloana1,collc[linia1][coloana1]);
                            deseneazaPiesa(linia2,coloana2,codPiesa);
                    }

        }

      }
      while (!mutareCorecta);
    }
  }
 }
 while (!mutareCorecta);
}

//STRATEGIE

int PiesaAleasa[6]={18,17,38,27,28,16};

int SaltCalculator2(int codPiesa, int &contorSaltCalculator,int indice)
{
    int l=PiesaAleasa[indice]/10;
    int c=PiesaAleasa[indice]%10;
    if(TablaDeJoc[l+2][c-2]==SPATIU&& TablaDeJoc[l+1][c-1]!=SPATIU)
    {
        Sleep(1000);
        TablaDeJoc[l][c]=SPATIU;
        TablaDeJoc[l+2][c-2]=codPiesa;
        stergePiesa(l,c,collc[l][c]);
        deseneazaPiesa(l+2,c-2,codPiesa);
        PiesaAleasa[indice]=PiesaAleasa[indice]+18;
        contorSaltCalculator++;

        return SaltCalculator2(codPiesa,contorSaltCalculator,indice);
    }
    else if(TablaDeJoc[l+2][c]==SPATIU&& TablaDeJoc[l+1][c]!=SPATIU)
    {
        Sleep(1000);
        TablaDeJoc[l][c]=SPATIU;
        TablaDeJoc[l+2][c]=codPiesa;
        stergePiesa(l,c,collc[l][c]);
        deseneazaPiesa(l+2,c,codPiesa);
        PiesaAleasa[indice]=PiesaAleasa[indice]+20;
        contorSaltCalculator++;

        return SaltCalculator2(codPiesa,contorSaltCalculator,indice);
    }
    else if(TablaDeJoc[l][c-2]==SPATIU&& TablaDeJoc[l][c-1]!=SPATIU)
    {
        Sleep(1000);
        TablaDeJoc[l][c]=SPATIU;
        TablaDeJoc[l][c-2]=codPiesa;
        stergePiesa(l,c,collc[l][c]);
        deseneazaPiesa(l,c-2,codPiesa);
        PiesaAleasa[indice]=PiesaAleasa[indice]-2;
        contorSaltCalculator++;

        return SaltCalculator2(codPiesa,contorSaltCalculator,indice);
    }
    else
        return contorSaltCalculator;
}
void Calculator(int ,int);
void PasCalculator1(int codPiesa, int indice)
{

    int l=PiesaAleasa[indice]/10;
    int c=PiesaAleasa[indice]%10;

    if(TablaDeJoc[l+1][c-1]==SPATIU)
    {
        Sleep(1000);
        TablaDeJoc[l][c]=SPATIU;
        TablaDeJoc[l+1][c-1]=codPiesa;
        stergePiesa(l,c,collc[l][c]);
        deseneazaPiesa(l+1,c-1,codPiesa);
        PiesaAleasa[indice]=PiesaAleasa[indice]+9;
    }
    else if(TablaDeJoc[l+1][c]==SPATIU)
    {
        Sleep(1000);
        TablaDeJoc[l][c]=SPATIU;
        TablaDeJoc[l+1][c]=codPiesa;
        stergePiesa(l,c,collc[l][c]);
        deseneazaPiesa(l+1,c,codPiesa);
        PiesaAleasa[indice]=PiesaAleasa[indice]+10;

    }
    else if(TablaDeJoc[l][c-1]==SPATIU)
    {
        Sleep(1000);
        TablaDeJoc[l][c]=SPATIU;
        TablaDeJoc[l][c-1]=codPiesa;
        stergePiesa(l,c,collc[l][c]);
        deseneazaPiesa(l,c-1,codPiesa);
        PiesaAleasa[indice]=PiesaAleasa[indice]-1;
    }
    else if(l==8&&c==4&&TablaDeJoc[l-1][c-1]==SPATIU)
    {
        Sleep(1000);
        TablaDeJoc[l][c]=SPATIU;
        TablaDeJoc[l-1][c-1]=codPiesa;
        stergePiesa(l,c,collc[l][c]);
        deseneazaPiesa(l-1,c-1,codPiesa);
        PiesaAleasa[indice]=PiesaAleasa[indice]-11;
    }
    else if(l==5&&c==1&&TablaDeJoc[l+1][c+1]==SPATIU)
    {
        Sleep(1000);
        TablaDeJoc[l][c]=SPATIU;
        TablaDeJoc[l+1][c+1]=codPiesa;
        stergePiesa(l,c,collc[l][c]);
        deseneazaPiesa(l+1,c+1,codPiesa);
        PiesaAleasa[indice]=PiesaAleasa[indice]+11;
    }
    else if(l==6&&c==2&&TablaDeJoc[l][c+1]==SPATIU)
    {
        Sleep(1000);
        TablaDeJoc[l][c]=SPATIU;
        TablaDeJoc[l][c+1]=codPiesa;
        stergePiesa(l,c,collc[l][c]);
        deseneazaPiesa(l,c+1,codPiesa);
        PiesaAleasa[indice]=PiesaAleasa[indice]+1;
    }
    else if(l==7&&c==3&&TablaDeJoc[l-1][c-1]==SPATIU)
    {
        Sleep(1000);
        TablaDeJoc[l][c]=SPATIU;
        TablaDeJoc[l-1][c-1]=codPiesa;
        stergePiesa(l,c,collc[l][c]);
        deseneazaPiesa(l-1,c-1,codPiesa);
        PiesaAleasa[indice]=PiesaAleasa[indice]-11;
    }
    else if((l==8&&c==1)||(l==8&&c==2)||(l==8&&c==3)||(l==7&&c==1)||(l==7&&c==2)||(l==6&&c==1))
        Calculator(codPiesa, indice+1);
    else Calculator(codPiesa, indice+1);

}


void Calculator(int codPiesa,int indicePiesa)
{
    if(indicePiesa>5)
        indicePiesa=0;
    int l=PiesaAleasa[indicePiesa]/10;
    int c=PiesaAleasa[indicePiesa]%10;
    int contorSaltCalculator=0;
    SaltCalculator2(codPiesa, contorSaltCalculator,indicePiesa);
    if(contorSaltCalculator==0)
        PasCalculator1(codPiesa,indicePiesa);

}


unsigned int PiesaAleasaSecund[6]={81,82,83,72,61,71};

int SaltCalculatorSecund(int codPiesa, int &contorSaltCalculator,int indice)
{
    int l=PiesaAleasaSecund[indice]/10;
    int c=PiesaAleasaSecund[indice]%10;
    if(TablaDeJoc[l-2][c+2]==SPATIU&& TablaDeJoc[l-1][c+1]!=SPATIU&&(l>1))
    {
        Sleep(1000);
        TablaDeJoc[l][c]=SPATIU;
        TablaDeJoc[l-2][c+2]=codPiesa;
        stergePiesa(l,c,collc[l][c]);
        deseneazaPiesa(l-2,c+2,codPiesa);
        PiesaAleasaSecund[indice]=PiesaAleasaSecund[indice]-18;
        contorSaltCalculator++;

        return SaltCalculatorSecund(codPiesa,contorSaltCalculator,indice);
    }
    else if(TablaDeJoc[l-2][c]==SPATIU&& TablaDeJoc[l-1][c]!=SPATIU&&(l>1))
    {
        Sleep(1000);
        TablaDeJoc[l][c]=SPATIU;
        TablaDeJoc[l-2][c]=codPiesa;
        stergePiesa(l,c,collc[l][c]);
        deseneazaPiesa(l-2,c,codPiesa);
        PiesaAleasaSecund[indice]=PiesaAleasaSecund[indice]-20;
        contorSaltCalculator++;

        return SaltCalculatorSecund(codPiesa,contorSaltCalculator,indice);
    }
    else if(TablaDeJoc[l][c+2]==SPATIU&& TablaDeJoc[l][c+1]!=SPATIU)
    {
        Sleep(1000);
        TablaDeJoc[l][c]=SPATIU;
        TablaDeJoc[l][c+2]=codPiesa;
        stergePiesa(l,c,collc[l][c]);
        deseneazaPiesa(l,c+2,codPiesa);
        PiesaAleasaSecund[indice]=PiesaAleasaSecund[indice]+2;
        contorSaltCalculator++;

        return SaltCalculatorSecund(codPiesa,contorSaltCalculator,indice);
    }
    else
        return contorSaltCalculator;
}
void CalculatorSecund(int ,int);
void PasCalculatorSecund(int codPiesa, int indice)
{

    int l=PiesaAleasaSecund[indice]/10;
    int c=PiesaAleasaSecund[indice]%10;

    if(TablaDeJoc[l-1][c+1]==SPATIU&&(l>1))
    {
        Sleep(1000);
        TablaDeJoc[l][c]=SPATIU;
        TablaDeJoc[l-1][c+1]=codPiesa;
        stergePiesa(l,c,collc[l][c]);
        deseneazaPiesa(l-1,c+1,codPiesa);
        PiesaAleasaSecund[indice]=PiesaAleasaSecund[indice]-9;
    }
    else if(TablaDeJoc[l-1][c]==SPATIU&&(l>1))
    {
        Sleep(1000);
        TablaDeJoc[l][c]=SPATIU;
        TablaDeJoc[l-1][c]=codPiesa;
        stergePiesa(l,c,collc[l][c]);
        deseneazaPiesa(l-1,c,codPiesa);
        PiesaAleasaSecund[indice]=PiesaAleasaSecund[indice]-10;

    }
    else if(TablaDeJoc[l][c+1]==SPATIU)
    {
        Sleep(1000);
        TablaDeJoc[l][c]=SPATIU;
        TablaDeJoc[l][c+1]=codPiesa;
        stergePiesa(l,c,collc[l][c]);
        deseneazaPiesa(l,c+1,codPiesa);
        PiesaAleasaSecund[indice]=PiesaAleasaSecund[indice]+1;
    }
    else if(l==1&&c==5&&TablaDeJoc[l+1][c+1]==SPATIU)
    {
        Sleep(1000);
        TablaDeJoc[l][c]=SPATIU;
        TablaDeJoc[l+1][c+1]=codPiesa;
        stergePiesa(l,c,collc[l][c]);
        deseneazaPiesa(l+1,c+1,codPiesa);
        PiesaAleasaSecund[indice]=PiesaAleasaSecund[indice]+11;
    }
    else if(l==2&&c==6&&TablaDeJoc[l+1][c+1]==SPATIU)
    {
        Sleep(1000);
        TablaDeJoc[l][c]=SPATIU;
        TablaDeJoc[l+1][c+1]=codPiesa;
        stergePiesa(l,c,collc[l][c]);
        deseneazaPiesa(l+1,c+1,codPiesa);
        PiesaAleasaSecund[indice]=PiesaAleasaSecund[indice]+11;
    }
    else if(l==3&&c==7&&TablaDeJoc[l][c-1]==SPATIU)
    {
        Sleep(1000);
        TablaDeJoc[l][c]=SPATIU;
        TablaDeJoc[l][c-1]=codPiesa;
        stergePiesa(l,c,collc[l][c]);
        deseneazaPiesa(l,c-1,codPiesa);
        PiesaAleasaSecund[indice]=PiesaAleasaSecund[indice]-1;
    }
    else if(l==4&&c==8&&TablaDeJoc[l-1][c-1]==SPATIU)
    {
        Sleep(1000);
        TablaDeJoc[l][c]=SPATIU;
        TablaDeJoc[l-1][c-1]=codPiesa;
        stergePiesa(l,c,collc[l][c]);
        deseneazaPiesa(l-1,c-1,codPiesa);
        PiesaAleasaSecund[indice]=PiesaAleasaSecund[indice]-11;
    }
    else if((l==1&&c==8)||(l==2&&c==8)||(l==3&&c==8)||(l==1&&c==7)||(l==2&&c==7)||(l==1&&c==6))
        CalculatorSecund(codPiesa, indice+1);
    else CalculatorSecund(codPiesa, indice+1);

}


void CalculatorSecund(int codPiesa,int indicePiesa)
{
    if(indicePiesa>5)
        indicePiesa=0;
    int l=PiesaAleasaSecund[indicePiesa]/10;
    int c=PiesaAleasaSecund[indicePiesa]%10;
    int contorSaltCalculator=0;
    SaltCalculatorSecund(codPiesa, contorSaltCalculator,indicePiesa);
    if(contorSaltCalculator==0)
        PasCalculatorSecund(codPiesa,indicePiesa);

}

//FINAL STRATEGIE....................................


void AfiseazaCastigator(int PunctajAlb, int PunctajNegru)// FUNCTIE CARE AFISEAZA LA FINALUL JOCULUI CASTIGATORUL SI PUNCTAJUL
{
       setcolor(RED);
        char Puncte[20]="Punctaj: ";
        int n;
        if(PunctajAlb>PunctajNegru)
            {
            outtextxy(1000,300,"Castigatorul: Negru");
            n=PunctajAlb-PunctajNegru;
            sprintf(Puncte + 9, "%d", n);
            outtextxy(1000,350,Puncte);
            }
        if(PunctajAlb==PunctajNegru)
        {
            outtextxy(1000,300,"Egalitate");
        }
        if(PunctajNegru>PunctajAlb)
        {
            outtextxy(1000,300,"Castigatorul: Alb");
            n=PunctajNegru-PunctajAlb;
            sprintf(Puncte+9,"%d",n);
            outtextxy(1000,350,Puncte);


        }
}

// DUPA CE SE TERMINA JOCUL APELAM FUNCTIA DE MAI JOS PENTRU A INCEPE UN NOU JOC SAU PENTRU A IESI DIN JOC
bool afiseazaCasutadeFinal()
{
    int mx,my;
    bool ok=1;


    setbkcolor(BLACK); clearviewport();
    settextstyle(10, HORIZ_DIR, 4);
    setcolor(YELLOW);
    settextjustify(CENTER_TEXT, CENTER_TEXT);



    outtextxy(600,200,"If you want to start a new game, press BACK TO MENU");
    outtextxy(600,250,"If you want to leave the game, press EXIT");

    CreareButonText(450,350,750,400,BLUE,RED,YELLOW,"Back to Menu");
    CreareButonText(450,450,750,500,BLUE,RED,YELLOW,"Exit");


                while(ok)
            if(ismouseclick(WM_LBUTTONDOWN))
                            {
                                clearmouseclick(WM_LBUTTONDOWN);
                                mx=mousex();
                                my=mousey();

                                if(mx>=350 && mx<=650 && my>=450 && my<=500)
                                return 0;

                                else if(mx>=350 && mx<=650 && my>=350 && my<=400)
                                 return 1;

                            }


}


int main()
{

    PlaySound(TEXT("gamemusic1.wav"), NULL,SND_ASYNC);// SOUND

    int w,h,PunctajAlb=0,PunctajNegru=0,indicePiesa=0 ,indicePiesaSecund=0;
    w=1200;
    h=800;

    int mx,my;
    bool ok;

   initwindow(w,h,"DinColtInColt");


    Intoarcere:

          Meniu(w,h);
        CreareButonText(430,250,800,300,BLUE,RED,YELLOW,"Play");
        CreareButonText(430,400,800,450,BLUE,RED,YELLOW,"Game Rules");
        CreareButonText(430,550,800,600,BLUE,RED,YELLOW,"Exit");





        ok=1;
    while (ok)
        if (ismouseclick(WM_LBUTTONDOWN))
        {
            clearmouseclick(WM_LBUTTONDOWN); //un singur click
            mx = mousex();
            my = mousey();
            if(mx>=430&&mx<=800&&my>=250&&my<=300) // PLAY
                {

                    Intoarcere2:

                    Meniu(w,h);
        CreareButonText(430,250,800,300,BLUE,RED,YELLOW,"Mode");
        CreareButonText(430,400,800,450,BLUE,RED,YELLOW,"Back");
        CreareButonText(430,550,800,600,BLUE,RED,YELLOW,"Exit");


            while (ok)
          if (ismouseclick(WM_LBUTTONDOWN))
          {
            clearmouseclick(WM_LBUTTONDOWN);
            mx = mousex();
            my = mousey();

            if(mx >= 430 && mx <= 800 && my >= 550 && my <= 600) // EXIT
                return 0;
            else if (mx >= 430 && mx <= 800 && my >= 400 && my <= 450) //BACK
            {
              goto Intoarcere;
            }
            else if (mx >= 430 && mx <= 800 && my >= 250 && my <= 300) //MODE

            {
            Intoarcere3:

                Meniu(w,h);
              CreareButonText(430,250,800,300,BLUE,RED,YELLOW,"Player vs Player");
              CreareButonText(430,400,800,450,BLUE,RED,YELLOW,"Player vs Computer");
              CreareButonText(430,550,800,600,BLUE,RED,YELLOW,"Back");

              while (ok)
                if (ismouseclick(WM_LBUTTONDOWN))
                {
                  clearmouseclick(WM_LBUTTONDOWN);
                  mx = mousex();
                  my = mousey();
                  if (mx >= 430 && mx <= 800 && my >= 550 && my <= 600) //BACK

                  {
                    goto Intoarcere2;
                  }
                  else if (mx >= 430 && mx <= 800 && my >= 250 && my <= 300) //PLAYER VS PLAYER
                  {

                      initTabla();
                      desen();
                         CreareButonText(800,700,950,750,BLUE,FUNDAL1,BLACK,"Change"); // schimba culoarea cand nu se mai vrea efectuarea saltului

                        int p=1,l,c,cod,la,ca;

                        while(!VictorieAlb() && !VictorieNegru())
                        {
                            if(p==1)
                            {
                                 mutarePiesa(PIESA1,cod,l,c,la,ca);
                                 if(cod==2 && verificspatiugol2(l,c,la,ca)==1)
                                 while(cod==2 && verificspatiugol2(l,c,la,ca)==1)
                                    mutarePiesa2(PIESA1,cod,l,c,la,ca);
                                    PunctajAlb++;
                                    AfisareNumarMutareAlb(PunctajAlb);
                                p=2;
                            }
                            else
                            {
                               mutarePiesa(PIESA2,cod,l,c,la,ca);
                                 if(cod==2 && verificspatiugol2(l,c,la,ca)==1)
                                 while(cod==2 && verificspatiugol2(l,c,la,ca)==1)
                                    mutarePiesa2(PIESA2,cod,l,c,la,ca);
                                    PunctajNegru++;
                                    AfisareNumarMutareNegru(PunctajNegru);
                                p=1;

                            }

                        }


                            while(!VictorieNegru())
                            {
                                mutarePiesa(PIESA2,cod,l,c,la,ca);
                                 if(cod==2 && verificspatiugol2(l,c,la,ca)==1)
                                 while(cod==2 && verificspatiugol2(l,c,la,ca)==1)
                                    mutarePiesa2(PIESA2,cod,l,c,la,ca);
                                    PunctajNegru++;
                                    AfisareNumarMutareNegru(PunctajNegru);
                            }


                            while(!VictorieAlb())
                            {
                                 mutarePiesa(PIESA1,cod,l,c,la,ca);
                                 if(cod==2 && verificspatiugol2(l,c,la,ca)==1)
                                 while(cod==2 && verificspatiugol2(l,c,la,ca)==1)
                                    mutarePiesa2(PIESA1,cod,l,c,la,ca);
                                    PunctajAlb++;
                                    AfisareNumarMutareAlb(PunctajAlb);
                            }

                            AfiseazaCastigator(PunctajAlb,PunctajNegru);

                            Sleep(4000);

                                     if(afiseazaCasutadeFinal()==0)
                            return 0;
                            else goto Intoarcere3;


                  }

                        else if(mx>=430 && mx<=800 && my>=400 && my<=450) // PLAYER VS COMPUTER
                            {

                                Meniu(w,h);


            CreareButonText(430,250,800,300,BLUE,RED,YELLOW,"WHITE");
              CreareButonText(430,400,800,450,BLUE,RED,YELLOW,"BLACK");
              CreareButonText(430,550,800,600,BLUE,RED,YELLOW,"Back");


                    while(ok)
                    if (ismouseclick(WM_LBUTTONDOWN))
                {
                  clearmouseclick(WM_LBUTTONDOWN);
                  mx = mousex();
                  my = mousey();
                  if (mx >= 430 && mx <= 800 && my >= 550 && my <= 600) //BACK

                  {
                    goto Intoarcere3;
                  }
                  else if(mx >= 430 && mx <= 800 && my >= 250 && my <= 300) //WHITE
                  {
                       initTabla();
                      desen();
                      CreareButonText(800,700,950,750,BLUE,FUNDAL1,BLACK,"Change"); // schimba culoarea cand nu se mai vrea efectuarea saltului

                        int p=1,l,c,cod,la,ca;

                        while(!VictorieAlb() && !VictorieNegru())
                        {
                            if(p==1)
                            {
                                 mutarePiesa(PIESA1,cod,l,c,la,ca);
                                 if(cod==2 && verificspatiugol2(l,c,la,ca)==1)
                                 while(cod==2 && verificspatiugol2(l,c,la,ca)==1)
                                    mutarePiesa2(PIESA1,cod,l,c,la,ca);
                                    PunctajAlb++;
                                    AfisareNumarMutareAlb(PunctajAlb);
                                p=2;
                            }
                            else
                            {
                               Calculator(PIESA2,indicePiesa);
                                if(indicePiesa==5)
                                    indicePiesa=0;
                                else
                                    indicePiesa++;

                                    PunctajNegru++;
                                    AfisareNumarMutareNegru(PunctajNegru);
                                p=1;
                            }

                        }
                                    while(!VictorieNegru())
                                    {
                                       Calculator(PIESA2,indicePiesa);
                                if(indicePiesa==5)
                                    indicePiesa=0;
                                else
                                    indicePiesa++;

                                    PunctajNegru++;
                                    AfisareNumarMutareNegru(PunctajNegru);
                                    }


                                    while(!VictorieAlb())
                                    {
                                        mutarePiesa(PIESA1,cod,l,c,la,ca);
                                        if(cod==2 && verificspatiugol2(l,c,la,ca)==1)
                                            while(cod==2 && verificspatiugol2(l,c,la,ca)==1)
                                                mutarePiesa2(PIESA1,cod,l,c,la,ca);
                                        PunctajAlb++;
                                        AfisareNumarMutareAlb(PunctajAlb);
                                    }

                                    AfiseazaCastigator(PunctajAlb,PunctajNegru);

                                    Sleep(4000);

                                     if(afiseazaCasutadeFinal()==0)
                            return 0;
                            else goto Intoarcere3;



                  }

                  else if(mx >= 430 && mx <= 800 && my >= 400 && my <= 450)// BLACK
                  {
                      initTabla();
                      desen();
                      CreareButonText(800,700,950,750,BLUE,FUNDAL1,BLACK,"Change"); // schimba culoarea cand nu se mai vrea efectuarea saltului

                        int p=1,l,c,cod,la,ca;

                        while(!VictorieAlb() && !VictorieNegru())
                        {
                            if(p==1)
                            {
                                 CalculatorSecund(PIESA1,indicePiesa);
                                if(indicePiesa==5)
                                    indicePiesa=0;
                                else
                                    indicePiesa++;

                                    PunctajAlb++;
                                    AfisareNumarMutareAlb(PunctajAlb);
                                p=2;
                            }
                            else
                            {
                               mutarePiesa(PIESA2,cod,l,c,la,ca);
                                 if(cod==2 && verificspatiugol2(l,c,la,ca)==1)
                                 while(cod==2 && verificspatiugol2(l,c,la,ca)==1)
                                    mutarePiesa2(PIESA2,cod,l,c,la,ca);
                                    PunctajNegru++;
                                    AfisareNumarMutareNegru(PunctajNegru);
                                p=1;
                            }

                        }
                                while(!VictorieNegru())
                            {
                                mutarePiesa(PIESA2,cod,l,c,la,ca);
                                 if(cod==2 && verificspatiugol2(l,c,la,ca)==1)
                                 while(cod==2 && verificspatiugol2(l,c,la,ca)==1)
                                    mutarePiesa2(PIESA2,cod,l,c,la,ca);
                                    PunctajNegru++;
                                    AfisareNumarMutareNegru(PunctajNegru);
                            }


                            while(!VictorieAlb())
                            {
                                 CalculatorSecund(PIESA1,indicePiesa);
                                if(indicePiesa==5)
                                    indicePiesa=0;
                                else
                                    indicePiesa++;

                                    PunctajAlb++;
                                    AfisareNumarMutareAlb(PunctajAlb);
                            }

                            AfiseazaCastigator(PunctajAlb,PunctajNegru);


                            Sleep(4000);

                                     if(afiseazaCasutadeFinal()==0)
                            return 0;
                            else goto Intoarcere3;

                  }


                    ok=0;
                    }

                        }
                    ok=0;
                }

            }

            }
            ok=0;
          }





            else if(mx >= 430 && mx <= 800 && my >= 400 && my <= 450) // GAME RULES
                    {
                         setbkcolor(BLACK);
                           clearviewport();

                           settextstyle(9,0,2);
                           settextjustify(1,2);
                           int NrLege=30;
                           outtextxy(600,25,"I.Jocul consta in deplasarea propriilor pioni in coltul opus in locul pionilor adversi si in");
                           outtextxy(600,50,"cat mai putine mutari");
                           outtextxy(600,75+NrLege,"II.Albul va incepe din coltul din stanga jos, iar negrul va incepe din coltul din dreapta sus");
                           outtextxy(600,100+NrLege*2,"III.Jucatorul poate deplasa oricare dintre pionii sai, prin pas simplu ori prin saritura");
                           outtextxy(600,125+NrLege*2,"a)pasul simplu consta in miscarea unei piese in orice directie cu o singura patratica ");
                           outtextxy(600,150+NrLege*2,"b) saritura consta in trecerea peste o alta piesa vecina( indiferent cui ii apartine), in linie dreapta");
                           outtextxy(600,175+NrLege*2,"(linie, coloana, diagonala), doar daca este liber patratul in care se doreste sa se ajunga.");
                           outtextxy(600,200+NrLege*2,"Aceasta saritura este permisa sa fie repetata cat timp se respecta regulile anterioare.");

                          CreareButonText(900,700,1000,750,BLUE,RED,YELLOW,"Back");
                          CreareButonText(1050,700,1150,750,BLUE,RED,YELLOW,"Exit");
                            CreareButonText(450,700,850,750,BLUE,RED,YELLOW,"Game demonstration");

                            while(ok)
                            if(ismouseclick(WM_LBUTTONDOWN))
                            {
                                clearmouseclick(WM_LBUTTONDOWN);
                                mx=mousex();
                                my=mousey();

                                if(mx>=900 && mx<=1000 && my>=700 && my<=750) //BACK

                                    goto Intoarcere;


                                else if (mx >= 1050 && mx <= 1150 && my >= 700 && my <= 750) //EXIT
                                    return 0;

                                else if(mx >= 450 && mx <= 850 && my >= 700 && my <= 750) // GAME DEMONSTRATION
                                {
                                    clearviewport();

                                    initTabla();
                                    desen();



                        int p=1,l,c,cod,la,ca;

                        while(!VictorieAlb() && !VictorieNegru())
                        {
                            if(p==1)
                            {
                                CalculatorSecund(PIESA1,indicePiesaSecund);
                                if(indicePiesaSecund==5)
                                    indicePiesaSecund=0;
                                else
                                    indicePiesaSecund++;

                                PunctajAlb++;
                                AfisareNumarMutareAlb2(PunctajAlb);
                                p=2;
                            }
                            else
                            {
                                Calculator(PIESA2,indicePiesa);
                            if(indicePiesa==5)
                                indicePiesa=0;
                            else
                                indicePiesa++;
                            PunctajNegru++;
                            AfisareNumarMutareNegru2(PunctajNegru);
                                p=1;
                            }

                        }
                        while(!VictorieNegru())
                        {
                            Calculator(PIESA2,indicePiesaSecund);
                            if(indicePiesaSecund==5)
                                indicePiesaSecund=0;
                            else
                                indicePiesaSecund++;
                            PunctajNegru++;
                            AfisareNumarMutareNegru2(PunctajNegru);


                        }


                        while(!VictorieAlb())
                        {
                            CalculatorSecund(PIESA1,indicePiesa);
                            if(indicePiesa==5)
                                indicePiesa=0;
                            else
                                indicePiesa++;

                            PunctajAlb++;
                            AfisareNumarMutareAlb2(PunctajAlb);
                        }

                        AfiseazaCastigator(PunctajAlb,PunctajNegru);
                        PunctajAlb=0;
                        PunctajNegru=0;

                        Sleep(4000);

                            if(afiseazaCasutadeFinal()==0)
                            return 0;
                            else goto Intoarcere;

                                }

                                    ok=0;
                            }
                    }

      else if (mx >= 430 && mx <= 800 && my >= 550 && my <= 600) //EXIT
        return 0;
      ok = 0;
}

   getch();
   closegraph();
   return 0;
}
